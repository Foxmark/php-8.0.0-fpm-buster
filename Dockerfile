FROM php:8.0.0-fpm-buster
WORKDIR "/var/www/"

RUN apt-get update \
    && apt-get -y --no-install-recommends install libxslt-dev libzip-dev \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN pecl install redis \
    && docker-php-ext-install calendar mysqli zip xsl pdo_mysql bcmath opcache \
    && docker-php-ext-enable redis \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

COPY config/99-php.ini /usr/local/etc/php/conf.d/99-php.ini
COPY config/20-xdebug.ini /usr/local/etc/php/conf.d/20-xdebug.ini

RUN chown -R www-data:www-data /var/www/
